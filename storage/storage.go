// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package storage

import (
	"log"
	"os"
	"time"

	"gitlab.com/pantacor/pantahub-aca/utils"

	"github.com/google/uuid"
	mgo "gopkg.in/mgo.v2"
)

const (
	collectionPrefix = "pantahub_aca_"
)

var (
	defaultStorage Storage
)

// Storage define all storage action and methods
type Storage interface {
	Close()

	IsNotFound(error) bool
	IsDuplicateKey(error) bool
}

type storage struct {
	session         *mgo.Session
	timeoutDuration time.Duration
}

func (s *storage) Close() {
	s.session.Close()
}

func (s *storage) IsNotFound(err error) bool {
	return err == mgo.ErrNotFound
}

func (s *storage) IsDuplicateKey(err error) bool {
	lastError, ok := err.(*mgo.LastError)
	if !ok {
		return false
	}

	return lastError.Code == 11000
}

func (s *storage) newID() string {
	return uuid.New().String()
}

func (s *storage) getCollection(name string) *mgo.Collection {
	name = collectionPrefix + name
	return s.session.DB("").C(name)
}

func (s *storage) getDbCollectionRaw(db, name string) *mgo.Collection {
	return s.session.DB(db).C(name)
}

// NewStorage create new Storage Struct
func NewStorage() Storage {
	if defaultStorage != nil {
		return defaultStorage
	}

	session, err := utils.GetMongoSession()
	if err != nil {
		panic(err)
	}

	if utils.GetEnv("PANTAHUB_ACA_DEBUG_MGO", "") != "" {
		mgo.SetLogger(log.New(os.Stdout, "", 0))
		mgo.SetDebug(true)
	}

	timeout, err := time.ParseDuration(utils.GetEnv("PANTAHUB_ACA_TIMEOUT_DURATION", "30m"))
	if err != nil {
		panic(err)
	}

	defaultStorage = &storage{session: session, timeoutDuration: timeout}
	return defaultStorage
}
