// Copyright (c) 2019  Pantacor Ltd.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.

package enroll

import (
	"net/http"

	"github.com/emicklei/go-restful"
	restfulspec "github.com/emicklei/go-restful-openapi"
)

// NewEnrollService create new enrollment service
func NewEnrollService() *restful.WebService {
	tags := []string{"enroll"}
	ws := new(restful.WebService)

	ws.Path("/enroll-request").
		Consumes(restful.MIME_JSON).
		Produces(restful.MIME_JSON)

	ws.Route(ws.POST("/").To(KeyEnroll).
		Doc("Enroll a new Key").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Operation("KeyEnroll").
		Reads(EnrollmentRequest{}).
		Writes(EnrollmentResponse{}).
		Returns(http.StatusOK, "OK", EnrollmentResponse{}))

	return ws
}
