FROM golang:alpine as builder

WORKDIR /go/src/gitlab.com/pantacor/pantahub-aca
COPY . .

RUN apk update; apk add git
RUN go get -d -v ./...
RUN go install -v ./...

FROM alpine

COPY env.default /opt/ph/bin/
COPY --from=builder /go/bin/pantahub-aca /opt/ph/bin/
COPY pantahub-fleet-docker-run /opt/ph/bin/
EXPOSE 12367

CMD [ "/opt/ph/bin/pantahub-fleet-docker-run" ]

RUN apk update; apk add ca-certificates
